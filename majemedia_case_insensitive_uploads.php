<?php

/*
Plugin Name: Case Insensitive Uploads
Plugin URI: https://www.majemedia.com/plugins/case-insensitive-uploads/
Description: Allows a site to apply case insensitivity or uploads year/month/day issues with urls submitted.
Version: 1.0
Author: Maje Media LLC
Author URI: https://www.majemedia.com/
Copyright: 2019
Text Domain: majemedia_case_insensitive_uploads
Domain Path: /lang
Requires PHP: 7.0
Tested up to 5.2.2
Stable Tag: 1.0
License: GPLv2 or later
License URI: https://www.gnu.org/licenses/gpl-2.0.html
*/

namespace majemedia_case_insensitive_uploads;

use majemedia_case_insensitive_uploads\Classes as Classes;

if( ! defined( 'ABSPATH' ) ) {
	die();
}

require_once( 'autoloader.php' );

class majemedia_case_insensitive_uploads extends Classes\Base {

	private static $instance;
	public         $Redirection;

	public static function get_instance() {

		if( ! self::$instance ) {
			self::$instance = new self();
		}

		return self::$instance;
	}

	public function __construct() {

		parent::__construct();

		$this->Redirection = new Classes\Redirection();

	}

}

$majemedia_case_insensitive_uploads = \majemedia_case_insensitive_uploads\majemedia_case_insensitive_uploads::get_instance();