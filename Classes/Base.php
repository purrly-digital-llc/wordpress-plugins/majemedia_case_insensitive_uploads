<?php

namespace majemedia_case_insensitive_uploads\Classes;

if( ! defined( 'ABSPATH' ) ) {
	die();
}

class Base {

	public $pluginPath;
	public $pluginURL;
	public $wpdb;
	public $pluginName                           = "Case Insensitive Upload";

	public function __construct() {

		global $wpdb;

		$this->pluginPath = realpath( dirname( __FILE__, 2 ) );
		$this->pluginURL  = plugins_url( basename( dirname( __FILE__, 2 ) ) );
		$this->wpdb       = $wpdb;

	}

}