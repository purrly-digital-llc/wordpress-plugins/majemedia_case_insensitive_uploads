<?php

namespace majemedia_case_insensitive_uploads\Classes;

if( ! defined( 'ABSPATH' ) ) {
	die();
}

class Redirection extends Base {

	public function __construct() {

		parent::__construct();

		add_action( 'template_redirect', [ $this, 'Redirect_404' ] );

	}

	public function Redirect_404() {

		if( ! is_404() ) {
			return;
		}

		$uploads_info            = wp_upload_dir();
		$content_directory_short = str_replace( get_home_url(), '', $uploads_info[ 'baseurl' ] );
		$request_matches_content = preg_match( "/^" . preg_quote( $content_directory_short, DIRECTORY_SEPARATOR ) . "/", $_SERVER[ 'REQUEST_URI' ] );

		if( $request_matches_content !== 1 ) {
			return;
		}

		$file_name = wp_basename( $_SERVER[ 'REQUEST_URI' ] );
		$args      = [
			'posts_per_page' => 1,
			'post_type'      => 'attachment',
			'post_name'      => trim( $file_name ),
		];

		$get_attachment = get_posts( $args );

		if( empty( $get_attachment ) ) {
			return;
		}

		// For the sake of simplicity we're going to return ONLY the first result.

		$attachment = $get_attachment[0];

		// This will work whether the guid property is a full uri or a contextual one.
		// @TODO add some settings to allow/disallow the redirection of incorrect subdirectory (2019/06 will redirect to 2019/07 if the file is found there)
		wp_redirect( $attachment->guid );

	}

}